SimpleSAMLphp Plugin for Privacy-Enhancing Protocols
===============================

# About this project

This is an open source plugin implementation for
SimpleSAMLphp that achieves several
privacy-enhancing protocols
proposed by the PEOFIAMP project.

For more information about PEOFIAMP, visit http://peofiamp.nii.ac.jp/

# License

The project is released under LGPL 2.1

> Copyright (C) 2013 Kyoto University
> 
> This library is free software; you can redistribute it and/or
> modify it under the terms of the GNU Lesser General Public
> License as published by the Free Software Foundation; either
> version 2.1 of the License, or (at your option) any later version.
>  
> This library is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
> Lesser General Public License for more details.
> 
> You should have received a copy of the GNU Lesser General Public
> License along with this library; if not, write to the Free Software
> Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA 

# How to Install

Please refer to INSTALL document.
