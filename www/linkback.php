<?php
/*
 * Copyright (C) 2013 Kyoto University
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA 
 */

/**
 * Handle linkback() response from IdP
 */

if (!array_key_exists('AuthState', $_REQUEST) || empty($_REQUEST['AuthState'])) {
  throw new SimpleSAML_Error_BadRequest('Missing state parameter on proxy-auth linkback endpoint.');
}
$stateID = $_REQUEST['AuthState'];

$state = SimpleSAML_Auth_State::loadState($stateID, sspmod_authproxy_Auth_Source_ProxyAuth::STAGE_INIT);

$proxyRole = $state[sspmod_authproxy_Auth_Source_ProxyAuth::PROXY_ROLE];
$asId = $state[sspmod_authproxy_Auth_Source_ProxyAuth::ASID];
SimpleSAML_Logger::debug("[ProxyAuth.linkback] asId=" . $asId);

$as = new SimpleSAML_Auth_Simple($asId);
$attributes = $as->getAttributes();
assert('is_array($attributes)');

$new_attributes = array();
if ($proxyRole === 'pxs') {
    $K2 = $state[sspmod_authproxy_Auth_Source_ProxyAuth::K2];
    SimpleSAML_Logger::debug("[ProxyAuth.linkback] Preparing binary k2. k2=" . $K2);
    $binK2 = pack("H*", $K2);
} else if ($proxyRole === 'pxi') {
    $K1 = $state[sspmod_authproxy_Auth_Source_ProxyAuth::K1];
    SimpleSAML_Logger::debug("[ProxyAuth.linkback] Preparing binary k1. k1=" . $K1);
    $binK1 = pack("H*", $K1);

    $spK1 = $state[sspmod_authproxy_Auth_Source_ProxyAuth::SP_K1];
    $new_attributes["urn:mace:gakunin.jp:idprivacy:attribute:cascade-sp-encrypted-k1"][] = $spK1;
}
foreach ($attributes as $org_key => $org_values) {
    if (strpos($org_key, "urn:mace:gakunin.jp:idprivacy:attribute", 0) === 0) {
        SimpleSAML_Logger::debug("[ProxyAuth.linkback] refrain encoding \"" . $org_key . "\"");
        foreach ($org_values as $value) {
            $new_attributes[$org_key][] = $value;
        }
        continue;
    }

    SimpleSAML_Logger::debug("[ProxyAuth.linkback] append encoded value for " . $org_key);
    foreach ($org_values as $value) {
        if ($proxyRole === 'pxs') {
            if (!ctype_xdigit($value)) {
                SimpleSAML_Logger::warning("[ProxyAuth.linkback] non-hex string \""
                                           . $value . "\"");
                $new_attributes[$org_key][] = $value;
                continue;
            }
            $binValue = pack("H*", $value);
            $j = 0;

            // TODO: is this really really fast enough??
            $out = '';
            for ($i = 0; $i < strlen($binValue); $i++) {
                $out .= $binValue{$i} ^ $binK2{$j};
                $j = ($j + 1) % strlen($binK2);
            }
            // $new_attributes[$org_key][] = $value;
            $new_attributes[$org_key][] = bin2hex($out);
        } else if ($proxyRole === 'pxi') {
            if (!ctype_xdigit($value)) {
                SimpleSAML_Logger::warning("[ProxyAuth.linkback] non-hex string \""
                                           . $value . "\"");
                $new_attributes[$org_key][] = $value;
                continue;
            }
            $binValue = pack("H*", $value);
            $j = 0;

            // TODO: merge two impls to one.
            // TODO: is this really really fast enough??
            $out = '';
            for ($i = 0; $i < strlen($binValue); $i++) {
                $out .= $binValue{$i} ^ $binK1{$j};
                $j = ($j + 1) % strlen($binK1);
            }
            // $new_attributes[$org_key][] = $value;
            $new_attributes[$org_key][] = bin2hex($out);
        } else {
            $new_attributes[$org_key][] = $value;
            // $new_attributes[$org_key][] = gethostname() . '(' . $value . ')';
        }
    }
}

$state['Attributes'] = $new_attributes;

SimpleSAML_Auth_Source::completeAuth($state);

/*
$config = SimpleSAML_Configuration::getInstance();
$t = new SimpleSAML_XHTML_Template($config, 'status.php', 'attributes');

$t->data['header'] = '{status:header_saml20_sp}';
$t->data['attributes'] = $attributes;
$t->data['logouturl'] = SimpleSAML_Utilities::selfURLNoQuery() . '?as=' . urlencode($asId) . '&logout';
$t->show();*/
