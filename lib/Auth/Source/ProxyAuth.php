<?php
/*
 * Copyright (C) 2013 Kyoto University
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA 
 */

class sspmod_authproxy_Auth_Source_ProxyAuth extends SimpleSAML_Auth_Source {
    const XML_NAMESPACE = 'urn:mace:gakunin.jp:idprivacy';
    const XML_ATTR_PROTOCOL_TYPE = 'protocol_type';
    const XML_ATTR_PROTOCOL_TYPE_DH = 'dh';
    const XML_ATTR_PROTOCOL_TYPE_CASCADE = 'cascade';
    const XML_ATTR_PROTOCOL_TYPE_AGENT = 'agent';

    const XML_ATTR_CASCADE_IDP_ENCRYPTED_K2 = 'cascade_idp_encrypted_k2';
    const XML_ATTR_CASCADE_PXI_K1 = 'cascade_pxi_k1';
    const XML_ATTR_CASCADE_SP_K1 = 'cascade_sp_k1';

    const XML_ATTR_DH_P = 'dh_p';
    const XML_ATTR_DH_G = 'dh_g';
    const XML_ATTR_DH_SP_PUB_KEY = 'dh_sp_pub_key';
    const XML_ATTR_DH_SP_ENCRYPTED_PRIV_KEY = 'dh_sp_encrypted_priv_key';

    const STAGE_INIT = 'proxyauth:init';
    const AUTHID = 'proxyauth:AuthId';
    const ASID = 'proxyauth:AsId';
    const K1 = 'proxyauth:K1';
    const SP_K1 = 'proxyauth:SpK1';
    const K2 = 'proxyauth:K2';
    const PROXY_ROLE = 'proxyauth:ProxyRole';

    const PRIVATE_KEY = 'proxyauth:PrivateKey';
    const PUBLIC_KEY = 'proxyauth:PublicKey';

    // For longer K2, we need longer RSA key on IdP.
    // (e.g. for K2 with 124 bits, we need 2048 bits RSA)
    const K2_LENGTH = 64;

    /*
     * Id for an authentication source specified in authsources.php
     */ 
    private $asId;
    
    /*
     * Can be "px" (DH), "pxs" (Cascade), or "pxi" (Cascade)
     */
    private $proxyRole;

    /*
     * URL that is used for obtaining extra information from SP.
     * For DH, dh_p, dh_g, dh_sp_pub_key, and dh_sp_encrypted_priv_key will be obtained
     * For Cascade, cascade_pxi_k1 and cascade_sp_k1 will be obtained.
     */
    private $spExtraDataUrl;

    /* The following data are Only effective inside pxi */
    private $publicKey;
    private $privateKey;
    private $privateKeyPassword;

    /**
     * Constructor for an authentication source.
     *
     * @param array $info  Information about this authentication source.
     * @param array &$config  Configuration for this authentication source.
     */
    public function __construct($info, &$config) {
        assert('is_array($info)');
        assert('is_array($config)');

        srand();

        /* Call the parent constructor first, as required by the interface. */
        parent::__construct($info, $config);

        // SimpleSAML_Logger::debug("[ProxyAuth] config=" . print_r($config, true));

        if (!array_key_exists('asId', $config)) {
            throw new Exception('Proxy authentication source is not properly configured: missing [asId]');
        }
        
        $this->asId = $config['asId'];
        $this->proxyRole = $config['proxyRole'];

        if ($this->proxyRole === 'pxs') {
            // TODO: It should not be statically loaded but dynamically fetched from metadata.
            $this->idpPublicKey = $config['idpPublicKey'];
            $this->spExtraDataUrl = $config['spExtraDataUrl'];
        } else if ($this->proxyRole === 'pxi') {
            $this->pxiPrivateKey = $config['pxiPrivateKey'];
            $this->pxiPrivateKeyPassword = $config['pxiPrivateKeyPassword'];
        } else if ($this->proxyRole === 'px') {
            $this->spExtraDataUrl = $config['spExtraDataUrl'];
        }

        SimpleSAML_Logger::debug("[ProxyAuth] asId=" . $this->asId
                                 . ", proxyRole=" . $this->proxyRole);
    }

    private function getFirstTextContentByTagName($dom, $tagName) {
        $list = $dom->getElementsByTagName($tagName);
        if ($list->length === 0) {
            SimpleSAML_Logger::error("[ProxyAuth] Failed to obtain \"" . $tagName . "\"");
            return null;
        } else if ($list->length > 1) {
            SimpleSAML_Logger::warning("[ProxyAuth] More than one parma available (" . $tagName . ")");
        }
        return $list->item(0)->textContent;
    }

    public function authenticate(&$state) {
        assert('is_array($state)');

        SimpleSAML_Logger::debug("[ProxyAuth] AuthId=" . $this->authId);

        // Note:
        // - In-coming extensions are instances of SAML2_XML_Chunk class.
        // - Out-going extensions must be instances which have toXML() method at least.
        $extensions = $state['saml:Extensions'];
        assert('is_array($extensions)');

        if ($this->proxyRole === 'pxs') {
            $extension_elems = array(self::XML_ATTR_PROTOCOL_TYPE => self::XML_ATTR_PROTOCOL_TYPE_CASCADE);

            // Read SP-generated K1 values, and store them in AuthnRequest.
            $sp_extra = file_get_contents($this->spExtraDataUrl);
            $sp_dom = new DOMDocument();
            if ($sp_dom->loadXML($sp_extra)) {
                $pxi_k1 = $this->getFirstTextContentByTagName($sp_dom, self::XML_ATTR_CASCADE_PXI_K1);
                $sp_k1 = $this->getFirstTextContentByTagName($sp_dom, self::XML_ATTR_CASCADE_SP_K1);
                if (isset($pxi_k1) && isset($sp_k1)) {
                    SimpleSAML_Logger::debug("[ProxyAuth] pxi_k1=\"" . $pxi_k1 . "\"");
                    SimpleSAML_Logger::debug("[ProxyAuth] sp_k1=\"" . $sp_k1 . "\"");
                    $extension_elems[self::XML_ATTR_CASCADE_PXI_K1] = $pxi_k1;
                    $extension_elems[self::XML_ATTR_CASCADE_SP_K1] = $sp_k1;
                }
            }

            $feed = '0123456789ABCDEF';
            $hexK2 = '';
            // TODO: check if this is fast enough.
            for ($i = 0; $i < self::K2_LENGTH; $i++) {
                $hexK2 .= $feed{rand(0, 15)};
            }
            assert(openssl_public_encrypt($hexK2, $idpEncryptedK2,
                                          'file://' . $this->idpPublicKey));
            $encodedK2 = bin2hex($idpEncryptedK2);
            $extension_elems[self::XML_ATTR_CASCADE_IDP_ENCRYPTED_K2] = $encodedK2;

            SimpleSAML_Logger::debug("[ProxyAuth] k2=\"" . $hexK2 . "\"");
            SimpleSAML_Logger::debug("[ProxyAuth] encoded_k2=\"" . $hexK2 ."\"");
            $state[self::K2] = $hexK2;
            $extensions[] = new sspmod_authproxy_Auth_Source_ProxyExtensions($extension_elems);
        } else if ($this->proxyRole === 'pxi') {
            // Prepare a new array for extensions. We'll replace the existing one with the new one.
            $new_extensions = array();
            $protocol_type_missing = true;

            // Iterate all the extension fields.
            //  - Some will be reused in $new_extensions as is.
            //  - Some will be removed when not needed.
            //  - New extension may be appended to $new_extensions instead of the original one.
            foreach ($extensions as $extension) {
                if ($extension->namespaceURI === self::XML_NAMESPACE
                    && $extension->localName === self::XML_ATTR_PROTOCOL_TYPE) {
                    if ($extension->getXML()->textContent !== self::XML_ATTR_PROTOCOL_TYPE_CASCADE) {
                        SimpleSAML_Logger::warning("[ProxyAuth] Protocol Type mismatch. "
                                                   . $extension->getXML()->textContent . " is not "
                                                   . self::XML_ATTR_PROTOCOL_TYPE_CASCADE);
                        $extension_elems =
                            array(self::XML_ATTR_PROTOCOL_TYPE => self::XML_ATTR_PROTOCOL_TYPE_CASCADE);
                        $new_extensions[] = new sspmod_authproxy_Auth_Source_ProxyExtensions($extension_elems);
                    } else {
                        $new_extensions[] = $extension;
                    }
                    $protocol_type_missing = false;
                } else if ($extension->namespaceURI === self::XML_NAMESPACE
                           && $extension->localName === self::XML_ATTR_CASCADE_PXI_K1) {
                    // K1 is encrypted by PxI's certificate.
                    // Decode it with PxI's private key
                    $priv_key = openssl_get_privatekey("file://" . $this->pxiPrivateKey,
                                                       $this->pxiPrivateKeyPassword);
                    $pxiK1 = $extension->getXML()->textContent;
                    SimpleSAML_Logger::debug("[ProxyAuth] pxi_k1=\"" . $pxiK1 . "\"");
                    $pxiEncryptedK1 = pack("H*", $pxiK1);
                    openssl_private_decrypt($pxiEncryptedK1, $rawK1, $priv_key);
                    SimpleSAML_Logger::debug("[ProxyAuth] k1=\"" . bin2hex($rawK1) . "\"");
                    $state[self::K1] = bin2hex($rawK1);

                    // We remove K1 from extensions field. IdP won't need it.
                } else if ($extension->namespaceURI === self::XML_NAMESPACE
                           && $extension->localName === self::XML_ATTR_CASCADE_SP_K1) {
                    $state[self::SP_K1] = $extension->getXML()->textContent;
                } else {
                    $new_extensions[] = $extension;
                }
            }
            if ($protocol_type_missing) {
                SimpleSAML_Logger::warning("[ProxyAuth] Protocol Type is missing. Assume it is Cascade.");
                $extension_elems = array(self::XML_ATTR_PROTOCOL_TYPE => self::XML_ATTR_PROTOCOL_TYPE_CASCADE);
                $new_extensions[] = new sspmod_authproxy_Auth_Source_ProxyExtensions($extension_elems);
            }
            $extensions = $new_extensions;
        } else if ($this->proxyRole === 'px') {
            $sp_extra = file_get_contents($this->spExtraDataUrl);
            $extension_elems = array(self::XML_ATTR_PROTOCOL_TYPE => self::XML_ATTR_PROTOCOL_TYPE_DH);
            $sp_dom = new DOMDocument();
            if ($sp_dom->loadXML($sp_extra)) {
                $sp_p = $this->getFirstTextContentByTagName($sp_dom, self::XML_ATTR_DH_P);
                $sp_g = $this->getFirstTextContentByTagName($sp_dom, self::XML_ATTR_DH_G);
                $sp_pub_key = $this->getFirstTextContentByTagName($sp_dom, self::XML_ATTR_DH_SP_PUB_KEY);
                $sp_encrypted_priv_key = $this->getFirstTextContentByTagName($sp_dom, self::XML_ATTR_DH_SP_ENCRYPTED_PRIV_KEY);
                if (isset($sp_p) && isset($sp_g) && isset($sp_pub_key) && isset($sp_encrypted_priv_key)) {
                    SimpleSAML_Logger::debug("[ProxyAuth] p=\"" . $sp_p . "\"");
                    SimpleSAML_Logger::debug("[ProxyAuth] g=\"" . $sp_g . "\"");
                    SimpleSAML_Logger::debug("[ProxyAuth] pub_key=\"" . $sp_pub_key . "\"");
                    SimpleSAML_Logger::debug("[ProxyAuth] e_priv_key=\"" . $sp_encrypted_priv_key . "\"");
                    $extension_elems[self::XML_ATTR_DH_P] = $sp_p;
                    $extension_elems[self::XML_ATTR_DH_G] = $sp_g;
                    $extension_elems[self::XML_ATTR_DH_SP_PUB_KEY] = $sp_pub_key;
                    $extension_elems[self::XML_ATTR_DH_SP_ENCRYPTED_PRIV_KEY] = $sp_encrypted_priv_key;
                }
            } else {
                SimpleSAML_Logger::error("[ProxyAuth] Failed to obtain DH params from DOM.");
            }
            $extensions[] = new sspmod_authproxy_Auth_Source_ProxyExtensions($extension_elems);
        } else {
            SimpleSAML_Logger::warning("[ProxyAuth] unexpected proxyRole \"" . $this->proxyRole . "\"");
        }

        // The authId will be needed to retrieve this authentication source later.
        $state[self::AUTHID] = $this->authId; 
        $state[self::ASID] = $this->asId;
        $state[self::PROXY_ROLE] = $this->proxyRole;
        $stateID = SimpleSAML_Auth_State::saveState($state, self::STAGE_INIT);

        $as = new SimpleSAML_Auth_Simple($this->asId);

        if ($as->isAuthenticated()) {
            SimpleSAML_Logger::debug("[ProxyAuth] already authorized.");
            SimpleSAML_Utilities::redirect(SimpleSAML_Module::getModuleURL('authproxy/linkback.php'),
                                           array('AuthState' => $stateID));
        } else {
            SimpleSAML_Logger::debug("[ProxyAuth] not authorized, yet.");
            $linkback = SimpleSAML_Module::getModuleURL('authproxy/linkback.php',
                                                        array('AuthState' => $stateID));
            $params = array(
                'ErrorURL' => $linkback,
                'ReturnTo' => $linkback,
            );
            $params['saml:Extensions'] = $extensions;
            $as->login($params);
        }
    }
}
