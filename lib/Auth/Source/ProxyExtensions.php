<?php
/*
 * Copyright (C) 2013 Kyoto University
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA 
 */

class sspmod_authproxy_Auth_Source_ProxyExtensions {

    private $elems;

    public function __construct($elems) {
        assert('is_array($elems)');
        $this->elems = $elems;
    }

    public function toXML(DOMElement $parent) {
        $doc = $parent->ownerDocument;
        foreach ($this->elems as $key => $value) {
            $e = $doc->createElementNS(sspmod_authproxy_Auth_Source_ProxyAuth::XML_NAMESPACE,
                                       $key, $value);
            $parent->appendChild($e);
        }
    }
}
